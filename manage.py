from flask_script import Manager, Server
#from flask_migrate import Migrate, MigrateCommand

from src.app import application, db

import sqlalchemy

#migrate = Migrate(application, db)
manager = Manager(application)


'''
# migrations
manager.add_command('db', MigrateCommand)
'''
# server route
server = Server()
manager.add_command("runserver", server)

# pytest
from tests.test import Test
manager.add_command("test", Test)

# Seed the database
from seed import SeedDataBase
manager.add_command("seed", SeedDataBase)

''' needs to be updated to remove flask.ext.script
#alchemydumps
from flask_alchemydumps import AlchemyDumps, AlchemyDumpsCommand
alchemydumps = AlchemyDumps(application, db)
manager.add_command('alchemydumps', AlchemyDumpsCommand)
'''

@manager.command
def create_db():
  """Creates the db tables."""
  db.create_all()
  print('success')


#rework this
@manager.command
def init_db():
  """Creates the database"""
  SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}:{port}".format(
    username="admin",
    password="XSQD89TZTam4ZtoQQE9y",
    port=3306,
    hostname="proj-bananabubble-db.ctjtxjm9uny6.us-east-1.rds.amazonaws.com",
    databasename="proj_bananabubble_db",
  )  
  engine = sqlalchemy.create_engine( SQLALCHEMY_DATABASE_URI)
  engine.execute("CREATE DATABASE proj_bananabubble_db")
  print('success')


if __name__ == '__main__':
  manager.run()
  #application.run(host="0.0.0.0", port=5000, debug = False)