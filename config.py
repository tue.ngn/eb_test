import os
#from basedir import basedir
basedir = os.path.abspath(os.path.dirname(__file__))

from dotenv import load_dotenv
load_dotenv()

#Do not include database config file in important uploads

env = os.getenv('ENV');

class LocalServerConfig(object):
  SECRET_KEY = "?TJLBDFWBFGVJVVB"
  DEBUG = True
  TESTING = True
  SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="root",
    password="",
    hostname="localhost",
    databasename="test2_database",
  )
  SQLALCHEMY_TRACK_MODIFICATIONS = True

class PythonAnywhereConfig(object):
  SECRET_KEY = "?TJLBDFWBFGVJVVB"
  DEBUG = True
  SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="noodlesprout",
    password="?tjlbdfwbfgvjvvb",
    hostname="noodlesprout.mysql.pythonanywhere-services.com",
    databasename="noodlesprout$default",
  )
  SQLALCHEMY_TRACK_MODIFICATIONS = True

class AWSConfig(object):
  SECRET_KEY = "?TJLBDFWBFGVJVVB"
  DEBUG = True
  SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}:{port}/{databasename}".format(
    username="admin",
    password="XSQD89TZTam4ZtoQQE9y",
    port=3306,
    hostname="proj-bananabubble-db.ctjtxjm9uny6.us-east-1.rds.amazonaws.com",
    databasename="proj_bananabubble_db",
  )
  SQLALCHEMY_TRACK_MODIFICATIONS = True

class TestingConfig(object):
  """Development configuration."""
  TESTING = True
  DEBUG = True
  WTF_CSRF_ENABLED = False
  SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
  DEBUG_TB_ENABLED = True
  PRESERVE_CONTEXT_ON_EXCEPTION = False

config_list = {
  'LocalServerConfig': LocalServerConfig,
  'PythonAnywhereConfig': PythonAnywhereConfig,
  'AWSConfig': AWSConfig
};

config = config_list[env]