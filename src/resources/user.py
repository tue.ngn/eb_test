from src.app import db
from src.models.user import User
from flask_restful import Resource, reqparse, Api
from flask import jsonify, Blueprint, request, make_response
from src.models.user import User
from flask_jwt_extended import (
  create_access_token,
  create_refresh_token,
  jwt_required,
  jwt_refresh_token_required,
  get_jwt_identity
  )

from marshmallow import Schema, fields
from marshmallow_sqlalchemy import ModelSchema
from webargs.flaskparser import use_args, use_kwargs, parser
from sqlalchemy.exc import IntegrityError

import logging
import json

logging.basicConfig(filename='app.log',level=logging.INFO, filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logging.info('resource user')

### clean up model schemas

'''
#model column name AND __init__ param names must be the same
#or add an additional param in schema
class UserSchema(ModelSchema):
  class Meta:
    model = User 
    sqla_session = db.Session
'''

class UserSchema(Schema):
  #id = fields.Int(dump_only=True)  # read-only (won't be parsed by webargs)
  email = fields.Str(required=True)
  password = fields.Str(required=True)
  first_name = fields.Str(missing="")
  last_name = fields.Str(missing="")
  role = fields.Str(missing="")

  #class Meta:
    #strict = True

#student_get_schema = UserSchema(exclude=['password_hashed'])
token_schema = UserSchema(only=('id', 'email')) #this comma space has to be there?
users_schema = UserSchema(many=True)
user_bp = Blueprint('user', __name__)
api = Api(user_bp)

class UserListSchema(Schema):
  class Meta:
    model = User 
    sqla_session = db.Session

class RequestSchema(Schema):
  page = fields.Int()

class PaginateSchema(Schema):
  items = fields.Nested(UserSchema, exclude=['password'], many=True)
  page = fields.Int()
  per_page = fields.Int()
  total = fields.Int()
  has_next = fields.Bool()
  has_prev = fields.Bool()
  next_num = fields.Int()
  prev_num = fields.Int()
  pages = fields.Int()

class UserSchema2(ModelSchema):
  class Meta:
    model = User

user_schema_2 = UserSchema2(exclude=('password', ))
user_schema_3 = UserSchema()

# not sure what this is for besides testing authorization using @jwt_required
class GetUser(Resource):
  @jwt_required
  def get(self):
    logging.info(request)

    return (jsonify({'data':'succuss'}))

def getData(request):
  return json.loads(request.data.decode("utf-8"))

class CreateUser(Resource):
  def post(self):
    args = UserSchema().dump(getData(request)).data

    user = User(
      email=args["email"],
      password=args["password"],
      first_name=args["first_name"],
      last_name=args["last_name"],
      role=args["role"] 
    )

    db.session.add(user)
    try:
      db.session.commit()
    except IntegrityError:
      db.session.rollback()
      return make_response(
        jsonify(message = "User with that email already exists"), 409
      )
    new_user = User.query.filter_by(email=args['email']).first()
    new_user_identity = token_schema.dump(new_user).data
    return jsonify(
      token=create_access_token(identity = new_user_identity)
    )

class GetUserInfo(Resource):
  @jwt_required
  @use_args(RequestSchema())
  def get(self,args):
    users = User.query.paginate(args['page'], 5, False).items
    payload = [user_schema_2.dump(user).data for user in users]
    return (jsonify(payload))


class DeleteUser(Resource):
  @jwt_required
  #@use_args(UserSchema())
  def delete(self):
    #logging.info(request.data)
    user_id = json.loads(request.data)['user_id']
    user = User.get_by_id(user_id)
    if not user:
      return make_response(
        jsonify(message = "User not found"),400
      )
    db.session.delete(user)
    try:
      db.session.commit()
      return make_response(
        jsonify(message = "User(s) successfully deleted from database"),200
      )      
    except IntegrityError:
      return make_response(
        jsonify(message = "Error has occured"), 400
      )

api.add_resource(GetUser, '/get_user')
api.add_resource(CreateUser, '/create_user')
api.add_resource(GetUserInfo, '/get_user_info')
api.add_resource(DeleteUser, '/delete_user')