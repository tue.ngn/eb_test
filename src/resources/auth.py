from src.models.user import User
from flask_restful import Resource, reqparse, Api
from flask import jsonify, Blueprint, request, make_response
import json

from flask_jwt_extended import (
  create_access_token,
  create_refresh_token,
  jwt_required,
  jwt_refresh_token_required,
  get_jwt_identity,
  get_raw_jwt,
  verify_jwt_in_request
  )

from marshmallow import Schema, fields
from webargs.flaskparser import use_args


class IdentitySchema(Schema):
  id = fields.Int()
  email = fields.Email()
  password = fields.Str()

token_schema = IdentitySchema(only=('id', 'email'))

auth_bp = Blueprint('auth', __name__)
api = Api(auth_bp)

def getData(request):
  return json.loads(request.data.decode("utf-8"))

class GetToken(Resource):
  #@use_args(IdentitySchema())
  def post(self):
    args = IdentitySchema().dump(getData(request)).data

    user = User.get_by_email_password(
      args["email"],
      args["password"]
    )
    user_identity, error = token_schema.dump(user)
    if user:
      return jsonify(token=create_access_token(identity = user_identity))
    else:
      return make_response(
        jsonify(error=True), 403
      )
      
class IsTokenValid(Resource):
  def post(self):
    verify_jwt_in_request()

class Dummy(Resource):
  def get(self):
    user = User.get_by_id(1)
    user_identity, error = token_schema.dump(user)
    return jsonify(user_identity)
    
api.add_resource(GetToken, '/get_token')
api.add_resource(IsTokenValid, '/is_token_valid')
api.add_resource(Dummy, '/dummy')

