from flask import Flask, redirect, render_template, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_restful import Api
from flask_jwt_extended import JWTManager

from config import config

#remove cors in production?
from flask_cors import CORS

# print a nice greeting.
def say_hello(username = "World"):
  return '<p>Hello %s!</p>\n' % username

# some bits of text for the page.
header_text = '''
  <html>\n<head> <title>EB Flask Test</title> </head>\n<body>'''
instructions = '''
  <p><em>Hint</em>: This is a RESTful web service! Append a username
  to the URL (for example: <code>/Thelonious</code>) to say hello to
  someone specific.</p>\n'''
home_link = '<p><a href="/">Back</a></p>\n'
footer_text = '</body>\n</html>'

# EB looks for an 'application' callable by default.
application = Flask(__name__)
application.config.from_object(config) #or PythonAnywhereConfig
db = SQLAlchemy(application)
bcrypt = Bcrypt(application)
api = Api(application)
jwt = JWTManager(application)
CORS(application)

# add a rule for the index page.
application.add_url_rule('/', 'index', (lambda: header_text +
  say_hello() + instructions + footer_text))

# add a rule when the page is accessed with a name appended to the site
# URL.
application.add_url_rule('/<username>', 'hello', (lambda username:
  header_text + say_hello(username) + home_link + footer_text))

# blueporints for restful api
from src.resources.auth import auth_bp
application.register_blueprint(auth_bp, url_prefix='/auth' )
from src.resources.user import user_bp
application.register_blueprint(user_bp, url_prefix='/user' )