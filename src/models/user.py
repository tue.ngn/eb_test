from src.app import bcrypt
from src.app import db
#from application.models.relationship.course_user import CourseUser

'''
Roles:
ADMIN
INSTRUCTOR
STUDENT 
'''


class User(db.Model):
  __tablename__   = 'user' 
  id          = db.Column(db.Integer(), primary_key=True)
  email       = db.Column(db.String(255), unique=True)
  password    = db.Column(db.String(255))
  first_name  = db.Column(db.String(20))
  last_name   = db.Column(db.String(20))
  role        = db.Column(db.String(20))

  def __init__(self, email, password, first_name, last_name, role):
    self.email = email
    self.password = User.hash_password(password)
    self.first_name = first_name
    self.last_name = last_name
    self.role = role

  def get_password(self):
    return self.password if self.password else None

  @staticmethod
  def get_by_id(id):
    user = User.query.filter_by(id=id).first()
    return user if user else None

  @staticmethod
  def get_by_email(email):
    user = User.query.filter_by(email=email).first()
    return user if user else None

  @staticmethod
  def hash_password(password):
    return bcrypt.generate_password_hash(password).decode("utf-8")

  @staticmethod
  def get_by_email_password(email, password):
    user = User.get_by_email(email)
    if user and bcrypt.check_password_hash(user.get_password(), password):
      return user
    else:
      return None

