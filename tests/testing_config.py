from flask_testing import TestCase
from src.app import db
from application import application
from src.models.user import User
#from config import TestingConfig

#models must be imported in order for db.create_all to work properly with relationship
import os
#from config import basedir
import json
import logging

logging.basicConfig(filename='app.log',level=logging.INFO, filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logging.info('testing config')

class BaseTestConfig(TestCase):
  default_user = {
    "email": "default@gmail.com",
    "password": "something2",
    "first_name": "Jack",
    "last_name": "doe",
    "role": "STUDENT"
  }

  def create_app(self):
    application.config.from_object('config.TestingConfig')
    return application

  def setUp(self):
    self.app = self.create_app().test_client()

    db.create_all()

    res = self.app.post(
        "/user/create_user",
        data=json.dumps(self.default_user),
        content_type='src/json'
    )
    self.token = json.loads(res.data.decode("utf-8"))["token"]


  def tearDown(self):
    db.session.remove()
    db.drop_all()


