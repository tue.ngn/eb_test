import sys
import os
import pytest
from config import basedir

import shutil

from flask_script import Command

#options = ['--cov-report=term','--cov-report=html', '--cov=application/', 'tests/']

class Test(Command):
  def run(self):
    #pytest.main(options)
    pytest.main(['tests/'])

    try:
      os.remove(os.path.join(basedir, '.coverage'))

    except OSError:
      pass

    try:
      shutil.rmtree(os.path.join(basedir, '.cache'))

    except OSError:
      pass

    try:
      shutil.rmtree(os.path.join(basedir, 'tests/.cache'))
    except OSError:
      pass



'''
def main():

    #argv = []
    #argv.extend(sys.argv[1:])
    #print('############ {}'.format(argv))

    pytest.main(options)

    try:
        os.remove(os.path.join(basedir, '.coverage'))

    except OSError:
        pass

    try:
        shutil.rmtree(os.path.join(basedir, '.cache'))

    except OSError:
        pass

    try:
        shutil.rmtree(os.path.join(basedir, 'tests/.cache'))
    except OSError:
        pass
'''


if __name__ == '__main__':
  main()
