
import logging
logging.basicConfig(filename='app.log',level=logging.INFO, filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logging.info('testing sandbox')

from marshmallow import Schema, fields

class UserSchema(Schema):
  email = fields.Str(required=True)
  password = fields.Str(required=True)
  first_name = fields.Str(missing="")
  last_name = fields.Str(missing="")
  role = fields.Str(missing="")

class TestSandBox():

  def test_marshmallow(self):
    data = {
      "email": "default@gmail.com",
      "password": "something2",
      "first_name": "Jack",
      "last_name": "doe",
      "role": "STUDENT"
    }
    
    schema = UserSchema()
    dump = schema.dump(data).data
