from testing_config import BaseTestConfig
from src.models.user import User
import json
from src.utils import auth
from flask import jsonify

import logging
logging.basicConfig(filename='app.log',level=logging.INFO, filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logging.info('testing api')

class TestAPI(BaseTestConfig):

  some_user = {
    "email": "janedoe@gmail.com",
    "password": "password",
    "first_name": "Jane",
    "last_name": "Doe",
    "role": "ADMIN"
  }

  def test_create_new_user(self):

    self.assertIsNone(User.query.filter_by(
      email=self.some_user["email"]
    ).first())
    
    res = self.app.post(
      "/user/create_user",
      data=json.dumps(self.some_user),
      content_type='application/json'
    )

    self.assertEqual(res.status_code, 200)
    self.assertTrue(json.loads(res.data.decode("utf-8"))["token"])
    self.assertEqual(User.query.filter_by(email=self.some_user["email"]).first().email, self.some_user["email"])

    res2 = self.app.post(
      "/user/create_user",
      data=json.dumps(self.some_user),
      content_type='application/json'
    )

    self.assertEqual(res2.status_code, 409)

  def test_get_token_and_verify_token(self):
    res = self.app.post(
      "/auth/get_token",
      data=json.dumps(self.default_user),
      content_type='application/json'
    )
    self.assertEqual(res.status_code, 200)
    token = json.loads(res.data.decode("utf-8"))["token"]
    res2 = self.app.post(
      "/auth/is_token_valid",
      data=json.dumps({"token": token}),
      content_type='application/json'
    )

    self.assertTrue(json.loads(res2.data.decode("utf-8")), ["token_is_valid"])

    res3 = self.app.post(
      "/auth/is_token_valid",
      data=json.dumps({"token": token + "something-else"}),
      content_type='application/json'
    )

    self.assertEqual(res3.status_code, 401)

    res4 = self.app.post(
      "/auth/get_token",
      data=json.dumps(self.some_user),
      content_type='application/json'
    )

    self.assertEqual(res4.status_code, 403)

  def test_protected_route(self):
    headers = {
      'Authorization': 'Bearer ' + self.token
    }
    bad_headers = {
      'Authorization': 'Bearer ' + self.token + "bad",
    }

    response = self.app.get('/user/get_user', headers=headers)
    self.assertEqual(response.status_code, 200)
    response3 = self.app.get('/user/get_user', headers=bad_headers)
    self.assertEqual(response3.status_code, 422)

