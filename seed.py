
from flask_script import Command

from src.app import db
from src.models.user import User

class SeedDataBase(Command):
  def run(self):
    db.drop_all()
    db.create_all()

    user1 = User(1,'janedoe@gmail.com','password','Jane','Doe','ADMIN')
    db.session.add(user1)

    try:
      db.session.commit()
      print('Seed complete')
    except IntegrityError:
      Print('seed failed')